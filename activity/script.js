async function retrieveData(){

	let results = await fetch('https://jsonplaceholder.typicode.com/todos')
	console.log(results)

	// console.log(typeof result);

	// console.log(result.body);

	let json = await results.json();
	// console.log(json);
		let result = Object.keys(json).map(key => [key, json[key]]);     for (let i = 0; i < result.length; i++) {         console.log(result[i][1].title);     } };


retrieveData();





//RETRIEVE A SINGLE TO DO LIST ITEM
fetch('https://jsonplaceholder.typicode.com/todos/1',{
  method: 'GET', 
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(),
})
.then((response)=> response.json)
.then((json)=> console.log(`The item "${json.title}" on  the list has a status of ${json.completed}`));


//CREATE TO DO LIST
fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New list',
		body: 'New to do list',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		Title: "",
		Description: "",
		Status: "",
		'Date Completed': "",
		userID:""
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		status:"Completed",
		'Date Completed': "now"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "DELETE"
});